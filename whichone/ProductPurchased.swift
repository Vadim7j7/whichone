//
//  ProductPurchased.swift
//  what-where-when
//
//  Created by Вадим on 06.11.14.
//  Copyright (c) 2014 Вадим. All rights reserved.
//

import UIKit
import StoreKit

typealias RequestProductsCompletionHandler = (success: Bool, products: NSArray!) -> ()
let IAPHelperProductPurchasedNotification = "IAPHelperProductPurchasedNotification"

class ProductPurchased: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    var _productsRequest:SKProductsRequest?
    var _completionHandler:RequestProductsCompletionHandler?
    var _productIdentifiers:NSSet = NSSet()
    var _purchasedProductIdentifiers:NSMutableSet?
    var _loaderView:UIView?
    
    func initWithProductIdentifiers(productIdentifiers:NSSet, loaderView:UIView) -> AnyObject {
        self._productIdentifiers = productIdentifiers as NSSet
        self._loaderView = loaderView
        self._purchasedProductIdentifiers = NSMutableSet()
        
        self._loaderView?.hidden = false
        for productIdentifier in productIdentifiers  {
            let productPurchased = NSUserDefaults.standardUserDefaults().boolForKey(productIdentifier as! String)
            if productPurchased {
                self._purchasedProductIdentifiers?.addObject(productIdentifier as! String)
            }
        }
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        
        return self
    }
    
    deinit {
        SKPaymentQueue.defaultQueue().removeTransactionObserver(self)
    }
    
    func requestProductsWithCompletionHandler(completionHandler:RequestProductsCompletionHandler) {
        self._completionHandler = completionHandler
        self._productsRequest = SKProductsRequest(productIdentifiers: self._productIdentifiers as! Set<String>)
        self._productsRequest?.delegate = self
        self._productsRequest?.start()
    }
    
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        self._loaderView?.hidden = true
        self._productsRequest = nil
        let skProducts = response.products
        for skProduct in skProducts {
            print("Found product: \(skProduct.productIdentifier) \(skProduct.localizedTitle) \(skProduct.price)")
        }
        
        self._completionHandler!(success: true, products: skProducts)
        self._completionHandler = nil
    }
    
    func request(request: SKRequest, didFailWithError error: NSError) {
        print("Failed to load list of products.")
        self._productsRequest = nil
        self._completionHandler!(success: false,products:  nil)
        self._completionHandler = nil
    }
    
    func productPurchased(productIdentifier:String) -> Bool {
        print("productPurchased")
        if let sss = self._purchasedProductIdentifiers {
            return sss.containsObject(productIdentifier)
        }
        return false
    }
    
    func buyProduct(product:SKProduct) {
        self._loaderView?.hidden = false
        let payment = SKPayment(product: product)
        SKPaymentQueue.defaultQueue().addPayment(payment)
    }
    
    func restoreCompletedTransactions() {
        self._loaderView?.hidden = false
        SKPaymentQueue.defaultQueue().restoreCompletedTransactions()
    }
    
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch (transaction as SKPaymentTransaction).transactionState {
            case .Purchased:
                self.completeTransaction(transaction as SKPaymentTransaction)
            case .Failed:
                self.failedTransaction(transaction as SKPaymentTransaction)
            case .Restored:
                self.completeRestoreTransaction(transaction as SKPaymentTransaction)
            default: continue
            }
        }
    }
    
    func paymentQueue(queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: NSError) {
        self._loaderView?.hidden = true
    }
    
    func completeTransaction(transaction:SKPaymentTransaction) {
        self._loaderView?.hidden = true
        self.provideContentForProductIdentifier(transaction.payment.productIdentifier)
        SKPaymentQueue.defaultQueue().finishTransaction(transaction)
    }
    
    func failedTransaction(transaction:SKPaymentTransaction) {
        self._loaderView?.hidden = true
        if transaction.error!.code != SKErrorPaymentCancelled {
            print("Transaction error: \(transaction.error!.localizedDescription)")
        }
        SKPaymentQueue.defaultQueue().finishTransaction(transaction)
    }
    
    func completeRestoreTransaction(transaction:SKPaymentTransaction) {
        self._loaderView?.hidden = true
        self.provideContentForProductIdentifier(transaction.originalTransaction!.payment.productIdentifier)
        SKPaymentQueue.defaultQueue().finishTransaction(transaction)
    }
    
    func provideContentForProductIdentifier(productIdentifier:String) {
        self._purchasedProductIdentifiers?.addObject(productIdentifier)
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: productIdentifier)
        NSUserDefaults.standardUserDefaults().synchronize()
        NSNotificationCenter.defaultCenter().postNotificationName(IAPHelperProductPurchasedNotification, object: productIdentifier, userInfo: nil)
    }
    
}