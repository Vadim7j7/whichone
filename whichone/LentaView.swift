//
//  LentaView.swift
//  whichone
//
//  Created by Вадим on 01.08.15.
//  Copyright (c) 2015 company. All rights reserved.
//

import Foundation
import UIKit

extension Array {
    subscript (safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
}

class LentaView:UIViewController, UITableViewDelegate, UITableViewDataSource {
    var _tableView:UITableView?
    var _sizes:[CGSize]?
    var _albums:[Album] = [Album]()

    override func viewDidLoad() {
        super.viewDidLoad()

        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self._sizes = [CGSizeMake(400, 350), CGSizeMake(300, 350)]
        } else {
            self._sizes = [CGSizeMake(250, 200), CGSizeMake(150, 200)]
        }

        self._tableView?.delegate = self
        self._tableView?.dataSource = self
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        _tableView?.reloadData();
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self._sizes![0].height
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _albums.count
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        _tableView?.reloadData();
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("idCell", forIndexPath: indexPath)
        
        for v in cell.subviews {
            if v.isKindOfClass(UIScrollView) {
                for ch in v.subviews {
                    if ch.isKindOfClass(UIView) {
                        (ch ).removeFromSuperview()
                    } else if ch.isKindOfClass(UIImageView) {
                        (ch as! UIImageView).removeFromSuperview()
                    }
                }
                (v as! UIScrollView).removeFromSuperview()
            }
        }
        
        let scroll = UIScrollView(frame: CGRectMake(0, 5, cell.frame.size.width, cell.frame.size.height-10))
        scroll.scrollEnabled = true
        scroll.showsHorizontalScrollIndicator = false
        
        let view = UIView(frame: CGRectMake(0, 0, 0, scroll.frame.size.height))
        view.backgroundColor = UIColor.whiteColor()
        
        if let photos = _albums[indexPath.row].photos {
            if photos.count > 0 {
                self.add_cell_images_wr(photos, i_photo: 0, i_album: indexPath.row, view: view, scroll: scroll)
            }
        }
        
        scroll.addSubview(view)
        cell.addSubview(scroll)
        
        return cell
    }
    
    private func add_cell_images_wr(photos:[Photo], i_photo:Int, i_album:Int, view:UIView, scroll:UIScrollView) {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self.add_cell_images(photos[i_photo], view: view, scroll: scroll, i_album: i_album, i_photo: i_photo) {
                () -> Void in
                
                if i_photo < photos.count-1 {
                    self.add_cell_images_wr(photos, i_photo: (i_photo+1), i_album: i_album, view: view, scroll: scroll)
                }
            }
        })
    }
    
    private func add_cell_images(photo:Photo, view:UIView, scroll:UIScrollView, i_album:Int, i_photo:Int, callback: () -> Void) {
        if (_albums[i_album].photos![safe: i_photo] != nil) {
            if let cashData = _albums[i_album].photos![i_photo].bufferData {
                dispatch_async(dispatch_get_main_queue(),{
                    self.gen_view_img(cashData, view: view, scroll: scroll, i_album: i_album, i_photo: i_photo)
                    callback()
                })
            } else {
                if let url = NSURL(string: photo.url_preview!) {
                    let request: NSURLRequest = NSURLRequest(URL: url)
                    NSURLConnection.sendAsynchronousRequest(
                        request, queue: NSOperationQueue.mainQueue(),
                        completionHandler: {(response: NSURLResponse?,data: NSData?,error: NSError?) -> Void in
                            if error == nil {
                                self.gen_view_img(data!, view: view, scroll: scroll, i_album: i_album, i_photo: i_photo)
                                
                                if (self._albums[safe: i_album] != nil) {
                                    if (self._albums[i_album].photos![safe: i_photo] != nil) {
                                        self._albums[i_album].photos![i_photo].bufferData = data
                                        callback()
                                    }
                                }
                            }
                    })
                }
            }
        }
    }

    private func gen_view_img(data:NSData, view:UIView, scroll:UIScrollView, i_album:Int, i_photo:Int) {
        var tmp_frame_view = view.frame
        let img = UIImage(data: data)
        let new_size:CGSize = img?.size.width > img?.size.height ? self._sizes![0] : self._sizes![1]
        let image = UIImageView(image: imageResize(image: img!, sizeChange: new_size))
        image.contentMode = UIViewContentMode.ScaleAspectFit

        var tmp_frame_image = image.frame
        tmp_frame_image.origin.x = tmp_frame_view.size.width + 5
        image.frame = tmp_frame_image
        
        let button = UIButton(frame: image.frame)
        button.addTarget(self, action: Selector("showFullPhoto:"), forControlEvents: UIControlEvents.TouchUpInside)
        button.tag = i_album
        button.titleLabel?.text = "\(i_photo)"
        
        tmp_frame_view.size.width += image.frame.size.width + 5
        view.frame = tmp_frame_view
        
        view.addSubview(button)
        view.addSubview(image)
        scroll.contentSize = view.frame.size
    }
}