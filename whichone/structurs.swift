//
//  structurs.swift
//  whichone
//
//  Created by Вадим on 17.07.15.
//  Copyright (c) 2015 company. All rights reserved.
//

import Foundation
import UIKit

enum UIUserInterfaceIdiom : Int {
    case Unspecified
    case Phone
    case Pad
}

struct Photo {
    private var _name:String?
    private var _url_preview:String?
    private var _url_original:String?
    private var _like:Int?
    private var _size:CGSize?
    private var _bufferData:NSData?

    var name:String? {
        set (n) {
            self._name = n
        }

        get {
            return self._name
        }
    }
    
    var url_preview:String? {
        set (u) {
            self._url_preview = u
        }

        get {
            return self._url_preview
        }
    }
    var url_original:String? {
        set (u) {
            self._url_original = u
        }
        
        get {
            return self._url_original
        }
    }
    var like:Int {
        set (l) {
            self._like = l
        }
        get {
            if let like = self._like {
                return like
            } else {
                return 0
            }
        }
    }
    var size:CGSize? {
        set (s) {
            self._size = s
        }
        get {
            return self._size
        }
    }
    
    var bufferData:NSData? {
        set(d) {
            self._bufferData = d
        }
        get {
            return self._bufferData
        }
    }
}

struct Album {
    private var _id:String?
    private var _device_id:String?
    private var _data_update:Float?
    private var _photos:[Photo]?

    init (_photos_: [[String: AnyObject]]?, path_photos:String) {
        if let photos: [[String: AnyObject]] = _photos_ {
            var buf = [Photo]()
            for _p_ in photos {
                var photo:Photo = Photo()
                let name = _p_["name"] as? String
                photo.name = name
                photo.url_preview = "\(path_photos)\(name!)-preview.jpeg"
                photo.url_original = "\(path_photos)\(name!)-original.jpeg"
                photo.like = (_p_["likes"] as? Int)!
                if let size: [String: CGFloat] = _p_["size"] as? [String: CGFloat] {
                    photo.size = CGSizeMake(size["w"]!, size["h"]!)
                }
                buf.append(photo)
            }
            self.photos = buf
        }
    }

    var album_id:String? {
        set(_id_) {
            self._id = _id_
        }
        get {
            return self._id
        }
    }
    
    var device_id:String? {
        set(d_id) {
            self._device_id = d_id
        }
        get {
            return self._device_id
        }
    }
    
    var data_update:Float? {
        set(date) {
            self._data_update = date
        }
        get {
            return self._data_update
        }
    }
    
    var photos:[Photo]? {
        set(data) {
            self._photos = data
        }
        get {
            return self._photos
        }
    }
}
