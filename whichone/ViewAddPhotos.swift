//
//  AddPhotos.swift
//  whichone
//
//  Created by Вадим on 23.07.15.
//  Copyright (c) 2015 company. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import StoreKit

class ViewAddPhotos: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var startLoader: UIView!
    @IBOutlet weak var bgLoader: UIView!
    @IBOutlet weak var btSendPhotos: UIButton!
    @IBOutlet weak var btSendPhotosPrivate: UIButton!
    @IBOutlet weak var labelLimitCount: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var bufer_data = [AnyObject]()
    var _api:Proxy_remote_db?
    var _productIdentifiers:NSSet = NSSet(objects: "which_one.company.org.1", "which_one.company.org.2", "which_one.company.org.3", "which_one.company.org.4", "which_one.company.org.5")
    let _productPurshs = ProductPurchased()
    var _products:AnyObject?
    var _limitConutPrivate:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.collectionView.delegate = self
        
        let device:UIDevice = UIDevice()
        let currentDeviceId:String = device.identifierForVendor!.UUIDString
        self._api = Proxy_remote_db(device_id: currentDeviceId)

        self._api?.gen_url("/", params: nil, auth: true) {(url: String) -> Void in}

        let swipeDown = UISwipeGestureRecognizer(target: self, action: "downSwipe")
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(swipeDown)
        
        _api?.get_vip_retai() {
            (count:Int) -> Void in
            
            if count > 0 {
                self._limitConutPrivate = count
                dispatch_async(dispatch_get_main_queue(),{
                    self.labelLimitCount.text = "\(count)"
                    self.labelLimitCount.layer.masksToBounds = true
                    self.labelLimitCount.layer.cornerRadius = 12
                })
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("productPurchased:"), name: IAPHelperProductPurchasedNotification, object: nil)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func downSwipe() {
        self.close()
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    override func shouldAutorotate() -> Bool {
        return false
    }

    func normalizedImage(image:UIImage) -> UIImage {
        if image.imageOrientation == UIImageOrientation.Up {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return normalizedImage
    }

    var _y_:CGFloat = 0.0
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let chosenImage:UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let newimage = UIImageView(image: self.normalizedImage(chosenImage))

            self.bufer_data.append(newimage)
            self.collectionView.reloadData()
            
            newimage.tag = self.bufer_data.count - 1
            
            if self.bufer_data.count > 0 {
                if self.bufer_data.count >= 2 {
                    self.btSendPhotos.hidden = false
                    self.btSendPhotosPrivate.hidden = false
                    self.labelLimitCount.hidden = false
                }
                startLoader.hidden = true
            } else {
                startLoader.hidden = false
            }
            
            picker.dismissViewControllerAnimated(true, completion: nil)
        }
    }

    @IBAction func ActinTakePhoto(sender: UIButton) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = false
        picker.mediaTypes = [String(kUTTypeImage)]
        picker.sourceType = UIImagePickerControllerSourceType.Camera

        self.presentViewController(picker, animated:true, completion:nil)
    }

    @IBAction func ActionSelectPhoto(sender: UIButton) {
        let imgPicker = UIImagePickerController()
        imgPicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imgPicker.modalPresentationStyle = UIModalPresentationStyle.Popover
        imgPicker.delegate = self
        imgPicker.allowsEditing = false
        imgPicker.mediaTypes = [String(kUTTypeImage)]
        self.presentViewController(imgPicker, animated: true, completion: nil)
    }

    func imagePickerControllerCancelled() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func ActionClose(sender: UIButton) {
        self.close()
    }

    @IBAction func ActionRemove(sender: UIButton) {
        self.bufer_data.removeAtIndex(sender.tag)
        self.collectionView.reloadData()

        if self.bufer_data.count < 2 {
            self.btSendPhotos.hidden = true
            self.btSendPhotosPrivate.hidden = true
            self.labelLimitCount.hidden = true
            
            if self.bufer_data.count <= 0 {
                startLoader.hidden = false
            }
        }
    }

    @IBAction func ActionSend(sender: UIButton) {
        self.sendFotoToServer()
    }
    
    @IBAction func ActionSendPrivate(sender: UIButton) {
        if self._limitConutPrivate <= 0 {
            let buyAlert = UIAlertController(title: NSLocalizedString("title_buy", comment: ""), message: NSLocalizedString("desc_buy", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
            let ff = NSLocalizedString("for", comment: "");
            
            self._productPurshs.initWithProductIdentifiers(self._productIdentifiers, loaderView: bgLoader)
            self._productPurshs.requestProductsWithCompletionHandler({
                (success, products) in
                if success {
                    self._products = products
                    self._products?.enumerateObjectsUsingBlock({
                        object, index, stop in
                        let product:SKProduct = object as! SKProduct
                        buyAlert.addAction(UIAlertAction(title: "\(product.localizedTitle)\(ff) \(self.LocalizedPrice(product))", style: .Default, handler: { (action: UIAlertAction!) in
                            self.onPay(product)
                        }));
                    })
                    self.showAlertBuy(buyAlert)
                }
            })
        } else {
            self.sendFotoToServer("1")
        }
    }
    func showAlertBuy(buyAlert:UIAlertController) {
        buyAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in}))
        presentViewController(buyAlert, animated: true, completion: nil)
    }
    func LocalizedPrice (product:SKProduct) -> String {
        let formatter = NSNumberFormatter ()
        formatter.formatterBehavior = NSNumberFormatterBehavior.Behavior10_4
        formatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        formatter.locale = product.priceLocale
        let formattedString = formatter.stringFromNumber(product.price)
        return formattedString!
    }
    func onPay(product:SKProduct) {
        self._productPurshs.buyProduct(product)
    }
    func productPurchased(notification:NSNotification) {
        let productIdentifier = notification.object as! String
        self._products?.enumerateObjectsUsingBlock({
            product, index, stop in
            
            if product.productIdentifier == productIdentifier {
                let myRegex = "[0-9]+"
                if let match = product.localizedTitle.rangeOfString(myRegex, options: .RegularExpressionSearch){
                    if let photos_count = Int(product.localizedTitle.substringWithRange(match)) {
                        self._limitConutPrivate = photos_count
                        dispatch_async(dispatch_get_main_queue(),{
                            self.labelLimitCount.text = "\(photos_count)"
                            self.labelLimitCount.layer.masksToBounds = true
                            self.labelLimitCount.layer.cornerRadius = 12
                            self.labelLimitCount.hidden = false
                        })
                        self._api?.set_vip("\(photos_count)")
                        self.sendFotoToServer("1")
                    }
                }
            }
        })
    }

    func sendFotoToServer(privatPhotos:String = "0") {
        self.bgLoader.hidden = false
        
        let hahs = rand_hash()
        var send_i = 0
        
        func send(data:NSData) {
            dispatch_async(dispatch_get_main_queue(), {
                self._api!.send_photos(data, album_id: hahs, privatePhotos: privatPhotos) {
                    () -> Void in
                    
                    send_i += 1
                    if send_i >= self.bufer_data.count {
                        self.btSendPhotos.hidden = true
                        self.btSendPhotosPrivate.hidden = true
                        self.labelLimitCount.hidden = true
                        self.close()
                    }
                }
            })
        }
        
        for photo in self.bufer_data {
            let imageData = UIImageJPEGRepresentation(((photo as? UIImageView)?.image)!, 0.1)
            send(imageData!)
        }
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bufer_data.count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell:AddPhotosPreview  = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as! AddPhotosPreview
        cell.photo.image = (self.bufer_data[indexPath.row] as! UIImageView).image
        cell.removePhoto.clipsToBounds = true
        cell.removePhoto.layer.masksToBounds = true
        cell.removePhoto.layer.cornerRadius = 17.5
        cell.removePhoto.tag = indexPath.row
        
        return cell
    }

    func close() {
        HOME_UPDATE = true
        self.dismissViewControllerAnimated(true) {
            () -> Void in
            self.bufer_data = []
            self.collectionView.reloadData()
        }
    }

    override func prefersStatusBarHidden() -> Bool {
        return false
    }

}