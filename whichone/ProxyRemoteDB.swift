//
//  ProxyRemoteDB.swift
//  whichone
//
//  Created by Вадим on 14.07.15.
//  Copyright (c) 2015 company. All rights reserved.
//

import Foundation


class Proxy_remote_db {
    private let _http:HTTP?;
    private let _host:String = "http://which-one.club";
    private let _api_key:String = "op3a1d2sd8df";
    private var _path_photos = "";
    private var _defaults_config:NSUserDefaults;
    private var _device_id:String?;
    private var _current_end_date_albom:Int?;
    private var _current_end_page_albom = 0;

    private var _current_page = 0;
    private var _prev_pages_count = 0;
    private var _end_date_album:String = "";

    init(device_id:String) {
        self._http = HTTP();
        self._path_photos = "\(self._host)/images/"
        self._defaults_config = NSUserDefaults.standardUserDefaults();
        self._device_id = nil;

        let utf8str = device_id.dataUsingEncoding(NSUTF8StringEncoding);
        if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)) {
            self._device_id = base64Encoded;
        }
    }

    func gen_url(path_api:String, params: [String: String]?, auth:Bool=false, callback: (String) -> Void) {
        var params_buf:String = "";
        if let _params = params {
            for (key, val) in _params {
                params_buf += "&\(key)=\(val)";
            }
        }

        if auth == true {
            var _id:String = "";
            var _hash:String = "";
            if self._defaults_config.objectForKey("_id_") != nil && self._defaults_config.objectForKey("_hash_") != nil {
                _id = (self._defaults_config.valueForKey("_id_") as? String)!;
                _hash = (self._defaults_config.valueForKey("_hash_") as? String)!;

                callback("\(self._host)/api/\(path_api)?key=\(self._api_key)\(params_buf)&id=\(_id)&hash=\(_hash)");
            } else {
                self.registration_device() {
                    (result) -> Void in
                    if result {
                        if self._defaults_config.objectForKey("_id_") != nil {
                            _id = (self._defaults_config.valueForKey("_id_") as? String)!;
                            _hash = (self._defaults_config.valueForKey("_hash_") as? String)!;

                            callback("\(self._host)/api/\(path_api)?key=\(self._api_key)\(params_buf)&id=\(_id)&hash=\(_hash)");
                        }
                    }
                }
            }
        } else {
            callback("\(self._host)/api/\(path_api)?key=\(self._api_key)\(params_buf)")
        }
    }

    private func registration_device(callback: (Bool) -> Void) {
        if let device_id = self._device_id {
            self.gen_url("registration", params: ["uid": device_id], auth: false) {
                (url: String) -> Void in

                self._http?.HTTPJSON(url, method: "POST") {
                    (_data: AnyObject?, error: String?) -> Void in
                    
                    if let data: AnyObject = _data {
                        if (data["status"] as? Int) == 200 {
                            if let body: AnyObject? = data["body"] {
                                if let _id = body!["id"] as? String {
                                    if let _hash = body!["hash"] as? String {
                                        self._defaults_config.setValue(_id, forKey: "_id_");
                                        self._defaults_config.setValue(_hash, forKey: "_hash_");
                                        callback(true);
                                    }
                                }
                            } else {
                                callback(false);
                            }
                        } else {
                            callback(false);
                        }
                    } else {
                        callback(false);
                    }
                }
                
            }
        }
    }
    
    func get_alboms(next:Bool=false, new_preload:Bool=false, last:Bool=false, callback: ([Album]) -> Void) -> Void {
        var params:[String: String]?
        var auth = false
        
        if next {
            if self._prev_pages_count > 0 {
                self._current_page += 1
                params = ["page": "\(self._current_page)"]
            } else {
                return
            }
        } else if new_preload {
            if self._end_date_album != "" {
                params = ["end_album_date": self._end_date_album]
            } else {
                return
            }
        } else if last {
            auth = true
            params = ["last": "1"]
        }

        self.gen_url("photos", params: params, auth: auth) {
            (url:String) -> Void in

            self._http?.HTTPJSON(url, method: "GET") {
                (_data: AnyObject?, error: String?) -> Void in
                
                if let response:[String: AnyObject] = _data as? [String: AnyObject] {
                    if (response["status"] as! Int) == 200 {
                        if let body: AnyObject = response["body"] {
                            var buf = [Album]()
                            if let data:NSArray = body["data"] as? NSArray {
                                if data.count > 0 {
                                    let tmp: AnyObject? = data.firstObject!["at_update"]!
                                    self._end_date_album = "\(tmp!)"
                                    for res in data {
                                        if let photos = res["photos"] {
                                            if photos!.count > 0 {
                                                var album:Album = Album(_photos_: (photos as! [[String: AnyObject]]), path_photos: self._path_photos)
                                                album.album_id = res["id_album"] as? String
                                                album.device_id = res["devices_id"] as? String
                                                album.data_update = res["at_update"] as? Float
                                                buf.append(album)
                                            }
                                        }
                                    }
                                }
                                self._prev_pages_count = buf.count
                            }
                            callback(buf)
                        }
                    }
                }
            }
        }
    }
    
    func get_photos_is_album(album_id:String?, callback: (Album) -> Void) {
        if let alb_id:String = album_id {
            self.gen_url("photos", params: ["album": alb_id], auth: false) {
                (url:String) -> Void in
                
                self._http?.HTTPJSON(url, method: "GET") {
                    (_data: AnyObject?, error: String?) -> Void in
                    
                    if let response:[String: AnyObject] = _data as? [String: AnyObject] {
                        if (response["status"] as! Int) == 200 {
                            if let body: AnyObject = response["body"] {
                                var buf:Album?
                                if let data:NSArray = body["data"] as? NSArray {
                                    if data.count > 0 {
                                        for res in data {
                                            if let photos = res["photos"] {
                                                if photos!.count > 0 {
                                                    var album:Album = Album(_photos_: (photos as! [[String: AnyObject]]), path_photos: self._path_photos)
                                                    album.album_id = res["id_album"] as? String
                                                    album.device_id = res["devices_id"] as? String
                                                    album.data_update = res["at_update"] as? Float
                                                    buf = album
                                                }
                                            }
                                        }
                                    }
                                    self._prev_pages_count = 1
                                }
                                callback(buf!)
                            }
                        }
                    }
                }
            }
        }
    }

    func send_photos(data:NSData, album_id:String = "", privatePhotos:String = "0", callbact: () -> Void) {
        self.gen_url("photos/add", params: ["album": album_id, "private_photos": privatePhotos], auth: true) {
            (url:String) -> Void in

            self._http?.HTTPJSON(url, data: data, method: "POST") {
                (_data: AnyObject?, error: String?) -> Void in
                callbact()
            }
        }
    }
    
    func send_like(photo_id:String, photo_name:String, callbact: () -> Void) {
        self.gen_url("photos/like", params: ["photo_id": photo_id, "name": photo_name], auth: true) {
            (url:String) -> Void in
            
            self._http?.HTTPJSON(url, method: "PUT") {
                (_data: AnyObject?, error: String?) -> Void in
                callbact()
            }
        }
    }
    
    func get_vip_retai(callbact: (Int) -> Void) {
        self.gen_url("vip/get", params: nil, auth: true) {
            (url:String) -> Void in
            
            self._http?.HTTPJSON(url, method: "GET") {
                (_data: AnyObject?, error: String?) -> Void in
                
                callbact((_data!["body"]! as! Int))
            }
        }
    }
    
    func set_vip(cout_albums:String) {
        self.gen_url("vip/set", params: ["count": cout_albums], auth: true) {
            (url:String) -> Void in
            
            self._http?.HTTPJSON(url, method: "PUT") {
                (_data: AnyObject?, error: String?) -> Void in
                
                print(_data)
            }
        }
    }

}
