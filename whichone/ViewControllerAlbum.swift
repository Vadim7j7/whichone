//
//  ViewControllerFullPhoto.swift
//  whichone
//
//  Created by Вадим on 18.07.15.
//  Copyright (c) 2015 company. All rights reserved.
//

import Foundation
import UIKit

class ViewControllerAlbum: UIViewController, UIPageViewControllerDataSource {
    @IBOutlet weak var topNavigaion: UIView!
    @IBOutlet weak var btClose: UIButton!
    @IBOutlet weak var viewView: UIView!

    var album: Album?
    var select_photo: Int?
    
    private var photosViewController: UIPageViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let _ = self.album {
            self.createPhotosViewController()
        }
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: "downSwipe")
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(swipeDown)
    }

    func downSwipe() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    @IBAction func onActionClose(sender: UIButton) {
        self.dismissViewControllerAnimated(true) {
            () -> Void in
        }
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    override func shouldAutorotate() -> Bool {
        return false
    }

    private func createPhotosViewController() {
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("pageViewContoller") as? UIPageViewController
        pageController!.dataSource = self

        if let _album_ = self.album {
            if let _photos_ = _album_.photos {
                if _photos_.count > 0 {
                    let firstController = getItemController(self.select_photo!)!
                    let startingViewControllers: NSArray = [firstController]
                    pageController!.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
                }
            }
        }

        self.photosViewController = pageController
        self.photosViewController!.view.frame = CGRectMake(0, 0, self.viewView.frame.size.width, self.viewView.frame.size.height)
        self.viewView.addSubview(self.photosViewController!.view)
    }

    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let itemController = viewController as! ViewControllerPhoto

        if itemController.itemIndex > 0 {
            return getItemController(itemController.itemIndex-1)
        }
        return nil
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let itemController = viewController as! ViewControllerPhoto
        if let _album_ = self.album {
            if let _photos_ = _album_.photos {
                if itemController.itemIndex+1 < _photos_.count {
                    return getItemController(itemController.itemIndex+1)
                }
            }
        }
        return nil
    }
    private func getItemController(itemIndex: Int) -> ViewControllerPhoto? {
        if let _album_ = self.album {
            if let _photos_ = _album_.photos {
                if itemIndex < _photos_.count {
                    let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("photoController") as! ViewControllerPhoto
                    pageItemController.itemIndex = itemIndex
                    pageItemController.albom = _album_

                    return pageItemController
                }
            }
        }
        return nil
    }
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        if let _album_ = self.album {
            if let _photos_ = _album_.photos {
                return _photos_.count
            }
        }
        return 0
    }
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.select_photo!
    }

}
