//
//  AddPhotosPreview.swift
//  whichone
//
//  Created by Вадим on 23.07.15.
//  Copyright (c) 2015 company. All rights reserved.
//

import Foundation
import UIKit

class AddPhotosPreview: UICollectionViewCell {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var countLike: UILabel!
    @IBOutlet weak var removePhoto: UIButton!

}