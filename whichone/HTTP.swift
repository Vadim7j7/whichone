//
//  HTTP.swift
//  rentalist
//
//  Created by Вадим on 18.04.15.
//  Copyright (c) 2015 Rentalist me. All rights reserved.
//

import Foundation

public class HTTP {
    private func HTTPsendRequest(request: NSMutableURLRequest, callback: (String, String?) -> Void) {
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {
            data, response, error in
            if error != nil {
                callback("", error!.localizedDescription);
            } else {
                callback(NSString(data: data!, encoding: NSUTF8StringEncoding)! as String, nil);
            }
        });
        task.resume();
    }
    
    private func JSONStringify(value: AnyObject, prettyPrinted: Bool = false) -> String {
        let options = NSJSONWritingOptions.PrettyPrinted
//        if prettyPrinted {
//            options = NSJSONWritingOptions.PrettyPrinted
//        } else {
//            options = nil
//        }
        if NSJSONSerialization.isValidJSONObject(value) {
            if let data = try? NSJSONSerialization.dataWithJSONObject(value, options: options) {
                if let string = NSString(data: data, encoding: NSUTF8StringEncoding) {
                    return string as String;
                }
            }
        }
        return "";
    }
    
    private func JSONParseDict(jsonString:String) -> Dictionary<String, AnyObject> {
        let data: NSData = jsonString.dataUsingEncoding(NSUTF8StringEncoding)!;
        let jsonObj = (try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions(rawValue: 0))) as! Dictionary<String, AnyObject>;
        
//        if (e != nil) {
//            return Dictionary<String, AnyObject>();
//        } else {
//            return jsonObj;
//        }
        return jsonObj;
    }
    
    
    func HTTPJSON(url: String, method: String = "GET", jsonObj: AnyObject? = nil, data: NSData? = nil, callback: (AnyObject?, String?) -> Void) {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!);
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData;

        if method == "POST" || method == "PUT" || method == "DELETE" {
            request.setValue("application/json", forHTTPHeaderField: "Content-Type");
            request.HTTPMethod = method;
            if let j_o: AnyObject = jsonObj {
                let jsonString = self.JSONStringify(j_o);
                let jsonData: NSData = jsonString.dataUsingEncoding(NSUTF8StringEncoding)!;
                request.HTTPBody = jsonData;
            } else if let df = data {
                let body = NSMutableData();
                body.appendData(df)
                request.setValue("\(body.length)", forHTTPHeaderField: "Content-Length")
                request.HTTPBody = body
            }
        } else {
            request.setValue("application/json", forHTTPHeaderField: "Accept");
        }

        self.HTTPsendRequest(request) {
            (data: String, error: String?) -> Void in
            
            if (error != nil) {
                callback(nil, error);
            } else {
                let jsonObj = self.JSONParseDict(data);

                callback(jsonObj, nil);
            }
        }
    }

}