//
//  ViewBlockPhotos.swift
//  whichone
//
//  Created by Вадим on 01.08.15.
//  Copyright (c) 2015 company. All rights reserved.
//

import Foundation
import UIKit

class ViewBlockPhotos:UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    var album: Album?
    var _voice_name: String?
    var _api:Proxy_remote_db?
    @IBOutlet weak var viewCollection: UICollectionView!
    @IBOutlet weak var btShare: UIButton!
    @IBOutlet weak var bgLoaderView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.album!.photos = self.sortPhotos(self.album!.photos!)

        if self._voice_name == nil {
        } else {
            self.btShare.hidden = true
        }
        self.viewCollection.dataSource = self
        
        let device:UIDevice = UIDevice()
        let currentDeviceId:String = device.identifierForVendor!.UUIDString
        self._api = Proxy_remote_db(device_id: currentDeviceId)
        _ = NSTimer.scheduledTimerWithTimeInterval(20, target:self, selector: Selector("tickTimeUpdate"), userInfo: nil, repeats: true)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return UIDevice.currentDevice().userInterfaceIdiom == .Pad ? CGSizeMake(180, 180) : CGSizeMake(149, 149);
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.album!.photos!.count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell:AddPhotosPreview  = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as! AddPhotosPreview
        
        if let photos = self.self.album!.photos {
            let image = UIImage(data: photos[indexPath.row].bufferData!)
            cell.photo.image = image
            cell.countLike.text = "\(photos[indexPath.row].like)"
            
            cell.countLike.clipsToBounds = true
            cell.countLike.layer.masksToBounds = true
            cell.countLike.layer.cornerRadius = 17.5
            
            if self._voice_name != nil && self._voice_name! == photos[indexPath.row].name {
                cell.countLike.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.75)
                cell.countLike.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                cell.countLike.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.75)
            }
        }

        return cell
    }
    
    @IBAction func actionBack(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func actionShare(sender: UIButton) {
        if let url = NSURL(string: (album?.photos![0].url_original)!) {
            let request: NSURLRequest = NSURLRequest(URL: url)
            self.bgLoaderView.hidden = false
            NSURLConnection.sendAsynchronousRequest(
                request, queue: NSOperationQueue.mainQueue(),
                completionHandler: {(response: NSURLResponse?,data: NSData?,error: NSError?) -> Void in
                    self.bgLoaderView.hidden = true
                    if error == nil {
                        if let image = UIImage(data: data!) {
                            let shareItems:Array = [
                                image,
                                NSLocalizedString("share", comment: ""),
                                NSURL(string: "http://which-one.club?\(self.album!.album_id!)")!
                            ];
                            let vc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
                            self.presentViewController(vc, animated: true, completion: nil)
                        }
                    }
            })
        }
    }
    
    func sortPhotos(photos:[Photo]) -> [Photo]? {
        var tmp = photos
        let len = tmp.count
        var i = 0
        while (i < len) {
            if(i == 0 || tmp[i - 1].like >= tmp[i].like) {
                i++
            } else {
                let temp = tmp[i]
                tmp[i] = tmp[i - 1]
                tmp[i - 1] = temp
                i--
            }
        }
        self.album!.photos = photos
        
        if self._voice_name == nil {
        } else {
            self.btShare.hidden = true
        }
        
        return tmp
    }
    
    func tickTimeUpdate() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self._api!.get_photos_is_album(self.album?.album_id) {
                (album:Album) -> Void in

                var i = 0
                for ph in album.photos! {
                    self.album!.photos![i].like = ph.like
                    i += 1
                }
                
                dispatch_async(dispatch_get_main_queue(),{
                    self.viewCollection.reloadData()
                })
            }
        }
    }
}