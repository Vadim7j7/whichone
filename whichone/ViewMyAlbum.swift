//
//  LastAlbum.swift
//  whichone
//
//  Created by Вадим on 26.07.15.
//  Copyright (c) 2015 company. All rights reserved.
//

import Foundation
import UIKit


class ViewMyAlbum:LentaView {
    var _api:Proxy_remote_db?
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let device:UIDevice = UIDevice()
        let currentDeviceId:String = device.identifierForVendor!.UUIDString
        self._api = Proxy_remote_db(device_id: currentDeviceId)
        
        self._tableView = tableView
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self._api!.get_alboms(false, new_preload: false, last: true) {
                (albums:[Album]) -> Void in
                
                self._albums = albums
                dispatch_async(dispatch_get_main_queue(),{
                    self._tableView!.reloadData()
                })
            }
        })
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    func showFullPhoto(sender:UIButton) {
        if let viewController:ViewBlockPhotos = self.storyboard?.instantiateViewControllerWithIdentifier("viewBlockAlbom") as? ViewBlockPhotos {
            viewController.album = self._albums[sender.tag]
            self.presentViewController(viewController, animated: true, completion: nil)
        }
    }
}