//
//  PageItemController.swift
//  Paging_Swift
//
//  Created by Olga Dalton on 26/10/14.
//  Copyright (c) 2014 swiftiostutorials.com. All rights reserved.
//

import UIKit
import CoreData

class ViewControllerPhoto: UIViewController {
    @IBOutlet weak var viewBgLoading: UIView!
    @IBOutlet weak var loadinIndicator: UIActivityIndicatorView!
    
    private var _albom: Album?
    private var _itemIndex: Int = 0
    private var imageView:UIImageView?
    private var managedObjectContext: NSManagedObjectContext?;

    var albom: Album? {
        set(al) {
            self._albom = al
        }
        get {
            return self._albom
        }
    }

    var itemIndex: Int {
        set(i) {
            self._itemIndex = i
        }
        get {
            return self._itemIndex
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        self.managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext;

        if let _albom_ = self._albom {
            if let _photos_ = _albom_.photos {
                self.viewBgLoading.layer.zPosition = 9
                self.viewBgLoading.hidden = false
                self.loadinIndicator.hidden = false
                self.loadinIndicator.startAnimating()

                self.imageView = UIImageView(frame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height))
                self.imageView?.contentMode = UIViewContentMode.ScaleAspectFit

                if let url = NSURL(string: (_photos_[self._itemIndex].url_original)!) {
                    let request: NSURLRequest = NSURLRequest(URL: url)
                    NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {
                        (response: NSURLResponse?,data: NSData?,error: NSError?) -> Void in

                        self.loadinIndicator.stopAnimating()
                        self.loadinIndicator.hidden = true
                        self.viewBgLoading.hidden = true
                        
                        self.imageView?.image = UIImage(data: data!)
                        self.view.addSubview(self.imageView!)
                        
                        var named_icon_bt:String?
                        let fetchRequest = NSFetchRequest(entityName: "Contacts")
                        fetchRequest.predicate = NSPredicate(format: "id_album BEGINSWITH [c]%@", _albom_.album_id!)
                        if let results: NSArray = try? self.managedObjectContext!.executeFetchRequest(fetchRequest) {
                            if results.count > 0 {
                                if (results[0] as! Contacts).name_photo == _photos_[self._itemIndex].name! {
                                    named_icon_bt = "ok"
                                }
                            } else {
                                named_icon_bt = "like"
                            }
                        }

                        
                        if named_icon_bt == nil || named_icon_bt == "ok" {
                            let _y_ = (self.view.frame.size.height - 15) / 2
                            let _x_ = (self.view.frame.size.width - 40) / 2
                            let like = UILabel(frame: CGRectMake(0, 0, 40, 40))
                            like.text = "\(_photos_[self._itemIndex].like)"
                            like.tintColor = UIColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 1)
                            like.font = UIFont.systemFontOfSize(24)
                            like.numberOfLines = 0
                            like.textAlignment = NSTextAlignment.Center
                            like.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
                            like.clipsToBounds = true
                            like.layer.masksToBounds = true
                            like.layer.cornerRadius = 20
                            like.frame = CGRectMake(_x_, _y_, like.frame.size.width, like.frame.size.height)
                            self.view.addSubview(like)
                        } else {
                            if let name_icon = named_icon_bt {
                                if let like_img = UIImage(named: name_icon) {
                                    let _x_ = (self.view.frame.size.width - like_img.size.width) / 2
                                    let _y_ = (self.view.frame.size.height - like_img.size.height) / 2
                                    let like_button = UIButton(frame: CGRectMake(_x_, _y_ + 10, like_img.size.width, like_img.size.height))
                                    like_button.setImage(like_img, forState: .Normal)
                                    
                                    if named_icon_bt == "like" {
                                        like_button.addTarget(self, action: Selector("send_like:"), forControlEvents: UIControlEvents.TouchUpInside)
                                        like_button.tag = self._itemIndex
                                    }
                                    
                                    self.view.addSubview(like_button)
                                }
                            }
                        }
                    })
                }
            }
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    func send_like(sender:UIButton) {
        if sender.tag <= -1 {
            return;
        }

        if var _albom_ = self._albom {
            if let _photos_ = _albom_.photos {
                if let _id_album = _albom_.album_id {
                    if let _name = _photos_[sender.tag].name {
                        self.loadinIndicator.startAnimating()
                        self.loadinIndicator.hidden = false
                        self.viewBgLoading.hidden = false

                        let fetchRequest = NSFetchRequest(entityName: "Contacts")
                        fetchRequest.predicate = NSPredicate(format: "id_album BEGINSWITH [c]%@", _albom_.album_id!)
                        if let results: NSArray = try? self.managedObjectContext!.executeFetchRequest(fetchRequest) {
                            if results.count <= 0 {

                                let device:UIDevice = UIDevice()
                                let currentDeviceId:String = device.identifierForVendor!.UUIDString
                                let _api = Proxy_remote_db(device_id: currentDeviceId)
                                
                                _api.send_like(_id_album, photo_name: _name) {
                                    () -> Void in

                                    let entityDescription = NSEntityDescription.insertNewObjectForEntityForName("Contacts", inManagedObjectContext: self.managedObjectContext!) as? Contacts;
                                    entityDescription?.id_album = _id_album
                                    entityDescription?.name_photo = _name
                                    do {
                                        try self.managedObjectContext?.save()
                                    } catch _ {
                                    }
                                    
                                    
                                    dispatch_async(dispatch_get_main_queue(),{
                                        sender.setImage(UIImage(named: "ok"), forState: .Normal)
                                        sender.tag = -1
                                        self.loadinIndicator.stopAnimating()
                                        self.loadinIndicator.hidden = true
                                        self.viewBgLoading.hidden = true
                                    });

                                    _albom_.photos![sender.tag].like += 1
                                    
                                    dispatch_async(dispatch_get_main_queue(),{
                                        if let viewController:ViewBlockPhotos = self.storyboard?.instantiateViewControllerWithIdentifier("viewBlockAlbom") as? ViewBlockPhotos {
                                            viewController.album = _albom_
                                            viewController._voice_name = _name
                                            self.presentViewController(viewController, animated: true, completion: nil)
                                        }
                                    })
                                }
                            } else {
                                self.loadinIndicator.stopAnimating()
                                self.loadinIndicator.hidden = true
                                self.viewBgLoading.hidden = true
                            }
                        }
                    }
                }
            }
        }
    }

}
