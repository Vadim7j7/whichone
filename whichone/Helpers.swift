//
//  Helpers.swift
//  whichone
//
//  Created by Вадим on 24.07.15.
//  Copyright (c) 2015 company. All rights reserved.
//

import Foundation
import UIKit

func randRange (lower: Int , upper: Int) -> Int {
    return lower + Int(arc4random_uniform(UInt32(upper - lower + 1)))
}

let _sss_ = ["a", "b", "c", "d", "e", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
func rand_hash() ->String {
    var buf = [String]()
    for _ in (0...45) {
        buf.append(_sss_[randRange(0, upper: 14)])
    }
    return buf.joinWithSeparator("")
}

func imageResize (image image:UIImage, sizeChange:CGSize)-> UIImage {
    
    let hasAlpha = true
    let scale: CGFloat = 0.0
    
    UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
    image.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
    
    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return scaledImage
}