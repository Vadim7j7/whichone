//
//  ViewController.swift
//  whichone
//
//  Created by Вадим on 14.07.15.
//  Copyright (c) 2015 company. All rights reserved.
//

import UIKit
import CoreData


class ViewController:LentaView {
    var _api:Proxy_remote_db?
    var _loadMoreStatus = false
    let _loaderFooter = UIActivityIndicatorView()
    var refreshControl: UIRefreshControl!
    @IBOutlet weak var tableAlbums: UITableView!
    private var managedObjectContext: NSManagedObjectContext?;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let device:UIDevice = UIDevice()
        let currentDeviceId:String = device.identifierForVendor!.UUIDString
        self._api = Proxy_remote_db(device_id: currentDeviceId)
        
        self.managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext;
        
        self.refreshControl = UIRefreshControl()
        self.tableAlbums.addSubview(self.refreshControl)
        self._tableView = tableAlbums
        
        self.reload_data()
    }
    
    
    func reload_data (reloadTb:Bool = true) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self._api?.get_alboms(false, new_preload: false) {
                (albums:[Album]) -> Void in
                
                self._albums = albums
                if (reloadTb) {
                    dispatch_async(dispatch_get_main_queue(),{
                        self._tableView!.reloadData()
                    });
                }
            }
        }
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if HOME_UPDATE {
            HOME_UPDATE = false
            self.scrollViewDidEndDecelerating(UIScrollView())
        }
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }

    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView(frame: CGRectMake(0, 0, 40, tableView.frame.size.width))
        self._loaderFooter.frame = CGRectMake(((tableView.frame.size.width - 25) / 2.0), ((40 - 25) / 2), 25, 25)
        self._loaderFooter.hidden = true
        self._loaderFooter.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        footer.addSubview(self._loaderFooter)

        return footer
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self._api?.get_alboms(false, new_preload: true) {
                (albums:[Album]) -> Void in
                
                if albums.count > 0 {
                    let buf = self._albums
                    self._albums = albums
                    self._albums.appendContentsOf(buf)
                    
                    dispatch_async(dispatch_get_main_queue(),{
                        self._tableView!.reloadData()
                    });
                }
                
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView!) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset <= 0 {
            loadMore()
        }
    }
    
    func loadMore() {
        if !self._loadMoreStatus {
            self._loadMoreStatus = true
//            self._loaderFooter.startAnimating()
//            self._loaderFooter.hidden = false
            
            loadMoreBegin({
//                self._loaderFooter.stopAnimating()
//                self._loaderFooter.hidden = true
                self._loadMoreStatus = false
                self._tableView?.reloadData()
            })
        }
    }
    func loadMoreBegin(loadMoreEnd:() -> ()){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self._api?.get_alboms(true, new_preload: false) {
                (albums:[Album]) -> Void in

                self._albums.appendContentsOf(albums)
                
                dispatch_async(dispatch_get_main_queue()) {
                    loadMoreEnd()
                }
            }
        }
    }
    
    func showFullPhoto(sender:UIButton) {
        var voice_photo:String?
        let fetchRequest = NSFetchRequest(entityName: "Contacts")
        fetchRequest.predicate = NSPredicate(format: "id_album BEGINSWITH [c]%@", self._albums[sender.tag].album_id!)
        if let results: NSArray = try? self.managedObjectContext!.executeFetchRequest(fetchRequest) {
            if results.count > 0 {
                voice_photo = (results[0] as! Contacts).name_photo
            }
        }

        if voice_photo == nil {
            if let _ = sender.titleLabel?.text {
                if let viewController:ViewControllerAlbum = self.storyboard?.instantiateViewControllerWithIdentifier("viewAlbum") as? ViewControllerAlbum {
                    viewController.album = self._albums[sender.tag]
                    viewController.select_photo = Int((sender.titleLabel?.text)!)
                    self.presentViewController(viewController, animated: true, completion: nil)
                }
            }
        } else {
            if let viewController:ViewBlockPhotos = self.storyboard?.instantiateViewControllerWithIdentifier("viewBlockAlbom") as? ViewBlockPhotos {
                viewController.album = self._albums[sender.tag]
                viewController._voice_name = voice_photo
                self.presentViewController(viewController, animated: true, completion: nil)
            }
        }
    }
}
